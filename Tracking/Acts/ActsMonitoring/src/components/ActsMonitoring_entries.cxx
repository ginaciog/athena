/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

#include "src/ActsSpacePointMonitoringAlgorithm.h"
#include "src/ActsSeedMonitoringAlgorithm.h"

DECLARE_COMPONENT( ActsTrk::ActsSpacePointMonitoringAlgorithm )
DECLARE_COMPONENT( ActsTrk::ActsSeedMonitoringAlgorithm )

